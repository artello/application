#!/bin/sh

set -e

echo "Artello Application Stack" > /etc/motd

echo 'root:art3ll0' | chpasswd

echo 'http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories

apk update
apk add bash shadow curl s6 go fuse git musl-dev python py-crcmod openssh

rc-update add s6
rc-update add s6-svscan
rc-update add sshd

curl --output google-cloud-sdk.tar.gz https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-257.0.0-linux-x86_64.tar.gz
tar -xvf google-cloud-sdk.tar.gz -C /var/lib

echo "export PAGER='more'" > /etc/profile.d/pager.sh
echo "export PATH=/var/lib/google-cloud-sdk/bin:$PATH" > /etc/profile.d/gcloud.sh

go get -v -u github.com/googlecloudplatform/gcsfuse
mv $HOME/go/bin/gcsfuse /usr/bin/gcsfuse

apk del go git musl-dev

mkdir -p /mnt/packages
mkdir -p $HOME/.ssh

keys="
  REPOS
  PUBLIC_KEY
  SSH_PUBLIC_KEY
"

for k in ${keys}; do
  export ${k}="$(curl -s --unix-socket /dev/lxd/sock x/1.0/config/user.${k})"
done

echo "$PUBLIC_KEY" > /etc/apk/keys/artello-builder.rsa.pub
echo "$REPOS" >> /etc/apk/repositories
echo "$SSH_PUBLIC_KEY" >> $HOME/.ssh/authorized_keys

